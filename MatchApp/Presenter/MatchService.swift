//
//  MatchService.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 13.11.21.
//

import Foundation

class MatchService {
    func deliverMatches(_ result: @escaping (_ matchModel: MatchModel) -> (),
                        _ failture: @escaping (_ error: ApiError) -> ()) {
        
        let matchInfoReq: GetMatchInfoRequest = GetMatchInfoRequest(proc: Constants.kGetMatchInfo, sport: 1, matchId: 1724836)
        
        ServerManager.shared.getMatchInfo(with: matchInfoReq) { matchModel in
            result(matchModel)
        } failtureBlock: { error in
            failture(error)
        }
    }
    
    func deliverVideoUrls(_ result: @escaping (_ videoList: [VideoUrlsModel]) -> (),
                        _ failture: @escaping (_ error: ApiError) -> ()) {

        let videoUrlsReq: VideoUrlsRequest = VideoUrlsRequest(matchId: 1724836, sportId: 1)
        ServerManager.shared.getVideoUrl(with: videoUrlsReq) { videoUrlList in
            result(videoUrlList)
        } failtureBlock: { error in
            failture(error)
        }
    }
}
