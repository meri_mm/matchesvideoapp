//
//  MatchPresenter.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 13.11.21.
//

import Foundation

class MatchPresenter {

    // MARK: - Private
    fileprivate let matchService: MatchService
    weak fileprivate var matchView: MatchView?

    init(matchService: MatchService){
        self.matchService = matchService
    }

    func attachView(_ attach: Bool, view: MatchView?) {
        if attach {
            matchView = nil
        } else {
            if let view = view { matchView = view }
        }
    }

    func getMatches(){
        self.matchView?.startLoading()
        matchService.deliverMatches { model in
            self.matchView?.finishLoading()
            self.matchView?.setMatchModel(model)
        } _: { error in
            self.matchView?.setEmpty()
        }
    }
    
    func getVideoUrls(){
        self.matchView?.startLoading()
        matchService.deliverVideoUrls { videoUrlList in
            self.matchView?.finishLoading()
            self.matchView?.setVideoUrls(videoUrlList)
        } _: { error in
            self.matchView?.setEmpty()
        }
    }
}
