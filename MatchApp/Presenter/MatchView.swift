//
//  MatchView.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 13.11.21.
//

import Foundation

protocol MatchView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setMatchModel(_ model: MatchModel)
    func setVideoUrls(_ list: [VideoUrlsModel])
    func setEmpty()
}
