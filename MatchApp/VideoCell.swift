//
//  VideoCell.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 13.11.21.
//

import UIKit

class VideoCell: UICollectionViewCell {
    static let cellIdentifier = "videoCell"

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
}
