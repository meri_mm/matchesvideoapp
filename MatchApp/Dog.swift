//
//  Dog.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 13.11.21.
//

import Foundation

enum Breed: String {
    case bulldog  = "Bulldog"
    case doberman = "Doberman"
    case labrador = "Labrador"
}

struct Dog {
    let name:  String
    let breed: String
    let age:   Int
}
