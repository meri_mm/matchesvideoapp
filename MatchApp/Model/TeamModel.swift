//
//  TeamModel.swift
//  Concierge
//
//  Created by Meri on 12/11/19.
//  Copyright © 2019 Meri. All rights reserved.
//

import UIKit

class TeamModel: BaseModel, NSCoding {
    // MARK: - Keys
    static let kId      = "id"
    static let kNameEng = "name_eng"
    static let kNameRus = "name_rus"
    static let kAbbrevEng = "abbrev_eng"
    static let kAbbrevRus = "abbrev_rus"
    static let kScore   = "score"
    
    // MARK: - Properties
    var id:         Int?
    var nameEng:    String?
    var nameRus:    String?
    var abbrevEng:  String?
    var abbrevRus:  String?
    var score:      Int?
    
    // MAddress = ARK: - Initialization
    override init(dictionary: [String : AnyObject]) {
        super.init(dictionary: dictionary)
        self.id         = intFromObject(dictionary[TeamModel.kId])
        self.nameEng    = stringFromObject(dictionary[TeamModel.kNameEng])
        self.nameRus    = stringFromObject(dictionary[TeamModel.kNameRus])
        self.abbrevEng  = stringFromObject(dictionary[TeamModel.kAbbrevEng])
        self.abbrevRus  = stringFromObject(dictionary[TeamModel.kAbbrevRus])
        self.score      = intFromObject(dictionary[TeamModel.kScore])
    }
    
    override init() {
        super.init()
        self.id         = 0
        self.nameEng    = ""
        self.nameRus    = ""
        self.abbrevEng  = ""
        self.abbrevRus  = ""
        self.score      = 0
    }
    
    init(model: TeamModel?) {
        super.init()
        self.id         = model?.id
        self.nameEng    = model?.nameEng
        self.nameRus    = model?.nameRus
        self.abbrevEng  = model?.abbrevEng
        self.abbrevRus  = model?.abbrevRus
        self.score      = model?.score
    }
    
    // MARK: - Methods
    override func dictionaryFromSelf() -> [String : AnyObject] {
        var dictionary = super.dictionaryFromSelf()
        dictionary[TeamModel.kId] = id as AnyObject?
        dictionary[TeamModel.kNameEng] = nameEng as AnyObject?
        dictionary[TeamModel.kNameRus] = nameRus as AnyObject?
        dictionary[TeamModel.kAbbrevEng] = abbrevEng as AnyObject?
        dictionary[TeamModel.kAbbrevRus] = abbrevRus as AnyObject?
        dictionary[TeamModel.kScore] = score as AnyObject?
        return dictionary
    }
    
    init(id: Int, nameEng: String, nameRus: String, abbrevEng: String, abbrevRus: String, score: Int) {
        super.init()
        self.id         = id
        self.nameEng    = nameEng
        self.nameRus    = nameRus
        self.abbrevEng  = abbrevEng
        self.abbrevRus  = abbrevRus
        self.score      = score
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        let id          = aDecoder.decodeObject(forKey: TeamModel.kId) as! Int
        let nameEng     = aDecoder.decodeObject(forKey: TeamModel.kNameEng) as! String
        let nameRus     = aDecoder.decodeObject(forKey: TeamModel.kNameEng) as! String
        let abbrevEng   = aDecoder.decodeObject(forKey: TeamModel.kAbbrevEng) as! String
        let abbrevRus   = aDecoder.decodeObject(forKey: TeamModel.kAbbrevRus) as! String
        let score       = aDecoder.decodeObject(forKey: TeamModel.kScore) as! Int
        self.init(id: id, nameEng: nameEng, nameRus: nameRus, abbrevEng: abbrevEng, abbrevRus:abbrevRus, score: score)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id        , forKey: TeamModel.kId       )
        aCoder.encode(nameEng   , forKey: TeamModel.kNameEng  )
        aCoder.encode(nameRus   , forKey: TeamModel.kNameRus  )
        aCoder.encode(abbrevEng , forKey: TeamModel.kAbbrevEng)
        aCoder.encode(abbrevRus , forKey: TeamModel.kAbbrevRus)
        aCoder.encode(score     , forKey: TeamModel.kScore    )
    }
}
