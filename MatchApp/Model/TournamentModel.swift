//
//  TournamentModel.swift
//  Concierge
//
//  Created by Meri on 12/11/19.
//  Copyright © 2019 Meri. All rights reserved.
//

import UIKit

class TournamentModel: BaseModel, NSCoding {
    // MARK: - Keys
    static let kId      = "id"
    static let kNameEng = "name_eng"
    static let kNameRus = "name_rus"
    
    // MARK: - Properties
    var id:         Int?
    var nameEng:    String?
    var nameRus:    String?
    
    // MAddress = ARK: - Initialization
    override init(dictionary: [String : AnyObject]) {
        super.init(dictionary: dictionary)
        self.id         = intFromObject(dictionary[TournamentModel.kId])
        self.nameEng    = stringFromObject(dictionary[TournamentModel.kNameEng])
        self.nameRus    = stringFromObject(dictionary[TournamentModel.kNameRus])
    }
    
    override init() {
        super.init()
        self.id         = 0
        self.nameEng    = ""
        self.nameRus    = ""
    }
    
    init(model: TournamentModel?) {
        super.init()
        self.id         = model?.id
        self.nameEng    = model?.nameEng
        self.nameRus    = model?.nameRus
    }
    
    // MARK: - Methods
    override func dictionaryFromSelf() -> [String : AnyObject] {
        var dictionary = super.dictionaryFromSelf()
        dictionary[TournamentModel.kId] = id as AnyObject?
        dictionary[TournamentModel.kNameEng] = nameEng as AnyObject?
        dictionary[TournamentModel.kNameRus] = nameRus as AnyObject?
        return dictionary
    }
    
    init(id: Int, nameEng: String, nameRus: String) {
        super.init()
        self.id         = id
        self.nameEng    = nameEng
        self.nameRus    = nameRus
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        let id          = aDecoder.decodeObject(forKey: TournamentModel.kId) as! Int
        let nameEng     = aDecoder.decodeObject(forKey: TournamentModel.kNameEng) as! String
        let nameRus     = aDecoder.decodeObject(forKey: TournamentModel.kNameEng) as! String
        self.init(id: id, nameEng: nameEng, nameRus: nameRus)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id        , forKey: TournamentModel.kId       )
        aCoder.encode(nameEng   , forKey: TournamentModel.kNameEng  )
        aCoder.encode(nameRus   , forKey: TournamentModel.kNameRus  )
    }
}
