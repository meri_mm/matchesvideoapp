//
//  MatchModel.swift
//  Concierge
//
//  Created by Meri on 12/11/19.
//  Copyright © 2019 Meri. All rights reserved.
//

import UIKit

class MatchModel: BaseModel, NSCoding {
    // MARK: - Keys
    static let kDate        = "date"
    static let kTournament  = "tournament"
    static let kTeam1       = "team1"
    static let kTeam2       = "team2"
    static let kCalc        = "calc"
    static let kHasVideo    = "has_video"
    static let kLive        = "live"
    static let kStorage     = "storage"
    static let kSub         = "sub"

    // MARK: - Properties
    var date      : String?
    var tournament: TournamentModel?
    var team1     : TeamModel?
    var team2     : TeamModel?
    var calc      : Bool?
    var hasVideo  : Bool?
    var live      : Bool?
    var storage   : Bool?
    var sub       : Bool?
    
    // MAddress = ARK: - Initialization
    override init(dictionary: [String : AnyObject]) {
        super.init(dictionary: dictionary)
        self.date       = stringFromObject(dictionary[MatchModel.kDate                  ])
        self.tournament = TournamentModel(dictionary: dictionary[MatchModel.kTournament ] as! [String : AnyObject])
        self.team1      = TeamModel(dictionary: dictionary[MatchModel.kTeam1            ] as! [String : AnyObject])
        self.team2      = TeamModel(dictionary: dictionary[MatchModel.kTeam2            ] as! [String : AnyObject])
        self.calc       = boolFromObject(dictionary[MatchModel.kCalc                    ])
        self.hasVideo   = boolFromObject(dictionary[MatchModel.kHasVideo                ])
        self.live       = boolFromObject(dictionary[MatchModel.kLive                    ])
        self.storage    = boolFromObject(dictionary[MatchModel.kStorage                 ])
        self.sub        = boolFromObject(dictionary[MatchModel.kSub                     ])
    }
    
    override init() {
        super.init()
        self.date       = ""
        self.tournament = TournamentModel()
        self.team1      = TeamModel()
        self.team2      = TeamModel()
        self.calc       = false
        self.hasVideo   = false
        self.live       = false
        self.storage    = false
        self.sub        = false
    }
    
    init(model: MatchModel?) {
        super.init()
        self.date       =  model?.date
        self.tournament =  model?.tournament
        self.team1      =  model?.team1
        self.team2      =  model?.team2
        self.calc       =  model?.calc
        self.hasVideo   =  model?.hasVideo
        self.live       =  model?.live
        self.storage    =  model?.storage
        self.sub        =  model?.sub
    }
    
    
    // MARK: - Methods
    override func dictionaryFromSelf() -> [String : AnyObject] {
        var dictionary = super.dictionaryFromSelf()
        dictionary[MatchModel.kDate      ] = date       as AnyObject?
        dictionary[MatchModel.kTournament] = tournament as AnyObject?
        dictionary[MatchModel.kTeam1     ] = team1      as AnyObject?
        dictionary[MatchModel.kTeam2     ] = team2      as AnyObject?
        dictionary[MatchModel.kCalc      ] = calc       as AnyObject?
        dictionary[MatchModel.kHasVideo  ] = hasVideo   as AnyObject?
        dictionary[MatchModel.kLive      ] = live       as AnyObject?
        dictionary[MatchModel.kStorage   ] = storage    as AnyObject?
        dictionary[MatchModel.kSub       ] = sub        as AnyObject?
        return dictionary
    }
    
    init(date : String, tournament: TournamentModel, team1 : TeamModel, team2 : TeamModel, calc : Bool, hasVideo : Bool, live : Bool, storage : Bool, sub : Bool) {
        super.init()
        self.date       =  date
        self.tournament =  tournament
        self.team1      =  team1
        self.team2      =  team2
        self.calc       =  calc
        self.hasVideo   =  hasVideo
        self.live       =  live
        self.storage    =  storage
        self.sub        =  sub
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        let date       = aDecoder.decodeObject(forKey:MatchModel.kDate      ) as! String
        let tournament = aDecoder.decodeObject(forKey:MatchModel.kTournament) as! TournamentModel
        let team1      = aDecoder.decodeObject(forKey:MatchModel.kTeam1     ) as! TeamModel
        let team2      = aDecoder.decodeObject(forKey:MatchModel.kTeam2     ) as! TeamModel
        let calc       = aDecoder.decodeObject(forKey:MatchModel.kCalc      ) as! Bool
        let hasVideo   = aDecoder.decodeObject(forKey:MatchModel.kHasVideo  ) as! Bool
        let live       = aDecoder.decodeObject(forKey:MatchModel.kLive      ) as! Bool
        let storage    = aDecoder.decodeObject(forKey:MatchModel.kStorage   ) as! Bool
        let sub        = aDecoder.decodeObject(forKey:MatchModel.kSub       ) as! Bool
        self.init(date : date, tournament: tournament, team1 : team1, team2 :team2, calc : calc, hasVideo : hasVideo, live :live, storage : storage, sub : sub)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(date       , forKey: MatchModel.kDate      )
        aCoder.encode(tournament , forKey: MatchModel.kTournament)
        aCoder.encode(team1      , forKey: MatchModel.kTeam1     )
        aCoder.encode(team2      , forKey: MatchModel.kTeam2     )
        aCoder.encode(calc       , forKey: MatchModel.kCalc      )
        aCoder.encode(hasVideo   , forKey: MatchModel.kHasVideo  )
        aCoder.encode(live       , forKey: MatchModel.kLive      )
        aCoder.encode(storage    , forKey: MatchModel.kStorage   )
        aCoder.encode(sub        , forKey: MatchModel.kSub       )
    }
}
