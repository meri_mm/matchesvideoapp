//
//  VideoModel.swift
//  Concierge
//
//  Created by Meri on 12/11/19.
//  Copyright © 2019 Meri. All rights reserved.
//

import UIKit

class VideoUrlsModel: BaseModel, NSCoding {
    // MARK: - Keys
    static let kName        = "name"
    static let kMatchId     = "match_id"
    static let kPeriod      = "period"
    static let kServerId    = "server_id"
    static let kQuality     = "quality"
    static let kFolder      = "folder"
    static let kVideoType   = "video_type"
    static let kAbc         = "abc"
    static let kStartMs     = "start_ms"
    static let kChecksum    = "checksum"
    static let kSize        = "size"
    static let kAbcType     = "abc_type"
    static let kDuration    = "duration"
    static let kFps         = "fps"
    static let kUrlRoot     = "url_root"
    static let kUrl         = "url"

    
    // MARK: - Properties
    var name        : String?
    var matchId     : Int?
    var period      : Int?
    var serverId    : Int?
    var quality     : String?
    var folder      : String?
    var videoType   : String?
    var abc         : String?
    var startMs     : Int?
    var checksum    : Int?
    var size        : Int?
    var abcType     : String?
    var duration    : Int?
    var fps         : Int?
    var urlRoot     : String?
    var url         : String?
    
    // MAddress = ARK: - Initialization
    override init(dictionary: [String : AnyObject]) {
        super.init(dictionary: dictionary)
        self.name        = stringFromObject(dictionary[VideoUrlsModel.kName     ])
        self.matchId     = intFromObject(dictionary[VideoUrlsModel.kMatchId     ])
        self.period      = intFromObject(dictionary[VideoUrlsModel.kPeriod      ])
        self.serverId    = intFromObject(dictionary[VideoUrlsModel.kServerId    ])
        self.quality     = stringFromObject(dictionary[VideoUrlsModel.kQuality  ])
        self.folder      = stringFromObject(dictionary[VideoUrlsModel.kFolder   ])
        self.videoType   = stringFromObject(dictionary[VideoUrlsModel.kVideoType])
        self.abc         = stringFromObject(dictionary[VideoUrlsModel.kAbc      ])
        self.startMs     = intFromObject(dictionary[VideoUrlsModel.kStartMs     ])
        self.checksum    = intFromObject(dictionary[VideoUrlsModel.kChecksum    ])
        self.size        = intFromObject(dictionary[VideoUrlsModel.kSize        ])
        self.abcType     = stringFromObject(dictionary[VideoUrlsModel.kAbcType  ])
        self.duration    = intFromObject(dictionary[VideoUrlsModel.kDuration    ])
        self.fps         = intFromObject(dictionary[VideoUrlsModel.kFps         ])
        self.urlRoot     = stringFromObject(dictionary[VideoUrlsModel.kUrlRoot  ])
        self.url         = stringFromObject(dictionary[VideoUrlsModel.kUrl      ])
    }
    
    override init() {
        super.init()
        self.name        = ""
        self.matchId     = 0
        self.period      = 0
        self.serverId    = 0
        self.quality     = ""
        self.folder      = ""
        self.videoType   = ""
        self.abc         = ""
        self.startMs     = 0
        self.checksum    = 0
        self.size        = 0
        self.abcType     = ""
        self.duration    = 0
        self.fps         = 0
        self.urlRoot     = ""
        self.url         = ""
    }
    
    init(model: VideoUrlsModel?) {
        super.init()
        self.name        = model?.name
        self.matchId     = model?.matchId
        self.period      = model?.period
        self.serverId    = model?.serverId
        self.quality     = model?.quality
        self.folder      = model?.folder
        self.videoType   = model?.videoType
        self.abc         = model?.abc
        self.startMs     = model?.startMs
        self.checksum    = model?.checksum
        self.size        = model?.size
        self.abcType     = model?.abcType
        self.duration    = model?.duration
        self.fps         = model?.fps
        self.urlRoot     = model?.urlRoot
        self.url         = model?.url
    }
    
    
    // MARK: - Methods
    override func dictionaryFromSelf() -> [String : AnyObject] {
        var dictionary = super.dictionaryFromSelf()
        dictionary[VideoUrlsModel.kName        ] = name        as AnyObject?
        dictionary[VideoUrlsModel.kMatchId     ] = matchId     as AnyObject?
        dictionary[VideoUrlsModel.kPeriod      ] = period      as AnyObject?
        dictionary[VideoUrlsModel.kServerId    ] = serverId    as AnyObject?
        dictionary[VideoUrlsModel.kQuality     ] = quality     as AnyObject?
        dictionary[VideoUrlsModel.kFolder      ] = folder      as AnyObject?
        dictionary[VideoUrlsModel.kVideoType   ] = videoType   as AnyObject?
        dictionary[VideoUrlsModel.kAbc         ] = abc         as AnyObject?
        dictionary[VideoUrlsModel.kStartMs     ] = startMs     as AnyObject?
        dictionary[VideoUrlsModel.kChecksum    ] = checksum    as AnyObject?
        dictionary[VideoUrlsModel.kSize        ] = size        as AnyObject?
        dictionary[VideoUrlsModel.kAbcType     ] = abcType     as AnyObject?
        dictionary[VideoUrlsModel.kDuration    ] = duration    as AnyObject?
        dictionary[VideoUrlsModel.kFps         ] = fps         as AnyObject?
        dictionary[VideoUrlsModel.kUrlRoot     ] = urlRoot     as AnyObject?
        dictionary[VideoUrlsModel.kUrl         ] = url         as AnyObject?
        return dictionary
    }
    
    init (name : String, matchId : Int, period : Int, serverId : Int, quality : String, folder : String, videoType : String, abc : String, startMs : Int, checksum : Int, size : Int, abcType : String, duration : Int, fps : Int, urlRoot : String, url : String) {
        super.init()
        self.name        = name
        self.matchId     = matchId
        self.period      = period
        self.serverId    = serverId
        self.quality     = quality
        self.folder      = folder
        self.videoType   = videoType
        self.abc         = abc
        self.startMs     = startMs
        self.checksum    = checksum
        self.size        = size
        self.abcType     = abcType
        self.duration    = duration
        self.fps         = fps
        self.urlRoot     = urlRoot
        self.url         = url
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let name        = aDecoder.decodeObject(forKey:VideoUrlsModel.kName      ) as! String
        let matchId     = aDecoder.decodeObject(forKey:VideoUrlsModel.kMatchId   ) as! Int
        let period      = aDecoder.decodeObject(forKey:VideoUrlsModel.kPeriod    ) as! Int
        let serverId    = aDecoder.decodeObject(forKey:VideoUrlsModel.kServerId  ) as! Int
        let quality     = aDecoder.decodeObject(forKey:VideoUrlsModel.kQuality   ) as! String
        let folder      = aDecoder.decodeObject(forKey:VideoUrlsModel.kFolder    ) as! String
        let videoType   = aDecoder.decodeObject(forKey:VideoUrlsModel.kVideoType ) as! String
        let abc         = aDecoder.decodeObject(forKey:VideoUrlsModel.kAbc       ) as! String
        let startMs     = aDecoder.decodeObject(forKey:VideoUrlsModel.kStartMs   ) as! Int
        let checksum    = aDecoder.decodeObject(forKey:VideoUrlsModel.kChecksum  ) as! Int
        let size        = aDecoder.decodeObject(forKey:VideoUrlsModel.kSize      ) as! Int
        let abcType     = aDecoder.decodeObject(forKey:VideoUrlsModel.kAbcType   ) as! String
        let duration    = aDecoder.decodeObject(forKey:VideoUrlsModel.kDuration  ) as! Int
        let fps         = aDecoder.decodeObject(forKey:VideoUrlsModel.kFps       ) as! Int
        let urlRoot     = aDecoder.decodeObject(forKey:VideoUrlsModel.kUrlRoot   ) as! String
        let url         = aDecoder.decodeObject(forKey:VideoUrlsModel.kUrl       ) as! String
        
        self.init(name : name, matchId : matchId, period : period, serverId : serverId, quality : quality, folder : folder, videoType : videoType, abc : abc, startMs : startMs, checksum : checksum, size : size, abcType : abcType, duration : duration, fps : fps, urlRoot : urlRoot, url : url)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name     , forKey:VideoUrlsModel.kName)
        aCoder.encode(matchId  , forKey:VideoUrlsModel.kMatchId)
        aCoder.encode(period   , forKey:VideoUrlsModel.kPeriod)
        aCoder.encode(serverId , forKey:VideoUrlsModel.kServerId)
        aCoder.encode(quality  , forKey:VideoUrlsModel.kQuality)
        aCoder.encode(folder   , forKey:VideoUrlsModel.kFolder)
        aCoder.encode(videoType, forKey:VideoUrlsModel.kVideoType)
        aCoder.encode(abc      , forKey:VideoUrlsModel.kAbc)
        aCoder.encode(startMs  , forKey:VideoUrlsModel.kStartMs)
        aCoder.encode(checksum , forKey:VideoUrlsModel.kChecksum)
        aCoder.encode(size     , forKey:VideoUrlsModel.kSize)
        aCoder.encode(abcType  , forKey:VideoUrlsModel.kAbcType)
        aCoder.encode(duration , forKey:VideoUrlsModel.kDuration)
        aCoder.encode(fps      , forKey:VideoUrlsModel.kFps)
        aCoder.encode(urlRoot  , forKey:VideoUrlsModel.kUrlRoot)
        aCoder.encode(url      , forKey:VideoUrlsModel.kUrl)
    }
}
