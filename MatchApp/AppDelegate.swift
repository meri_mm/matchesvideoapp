//
//  AppDelegate.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 12.11.21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let matchInfoReq: GetMatchInfoRequest = GetMatchInfoRequest(proc: Constants.kGetMatchInfo, sport: 1, matchId: 1724836)
        ServerManager.shared.getMatchInfo(with: matchInfoReq) { matchModel in
            
        } failtureBlock: { Error in
            
        }
        
        let videoUrlsReq: VideoUrlsRequest = VideoUrlsRequest(matchId: 1724836, sportId: 1)
        ServerManager.shared.getVideoUrl(with: videoUrlsReq) { videoUrlModel in
            
        } failtureBlock: { error in
            
        }


        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

