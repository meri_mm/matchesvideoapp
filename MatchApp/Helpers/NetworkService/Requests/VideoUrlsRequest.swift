//
//  VideoUrlsRequest.swift
//  Concierge
//
//  Created by Meri on 12/19/19.
//  Copyright © 2019 Meri. All rights reserved.
//

import UIKit

class VideoUrlsRequest: BaseModel {
    
    // MARK: - Keys
    static let kMatchId = "match_id"
    static let kSportId = "sport_id"
    
    // MARK: - Properties
    var data: Dictionary<String, Any> = [:]
    var matchId: Int
    var sportId: Int
    
    
    // MARK: - Initialization
    init(matchId: Int, sportId: Int) {
        self.matchId = matchId
        self.sportId = sportId
        
        data[VideoUrlsRequest.kMatchId] = matchId
        data[VideoUrlsRequest.kSportId] = sportId
        super.init()
    }
    
    // MARK: - Methods
    override func dictionaryFromSelf() -> [String : AnyObject] {
        return self.data as [String : AnyObject]
    }
}
