//
//  GetMatchInfoRequest.swift
//  Concierge
//
//  Created by Meri on 12/19/19.
//  Copyright © 2019 Meri. All rights reserved.
//

import UIKit

class GetMatchInfoRequest: BaseModel {
    
    // MARK: - Keys
    static let kProc = "proc"
    static let kParams = "params"
    static let kSport = "_p_sport"
    static let kMatchId = "_p_match_id"
    
    // MARK: - Properties
    var data: Dictionary<String, Any> = [:]
    var proc: String
    var sport: Int
    var matchId: Int
    
    
    // MARK: - Initialization
    init(proc: String, sport: Int, matchId: Int) {
        self.proc = proc
        self.sport = sport
        self.matchId = matchId
        
        data[GetMatchInfoRequest.kProc] = proc
        
        var param: Dictionary<String, Int> = [:]
        param[GetMatchInfoRequest.kSport] = sport
        param[GetMatchInfoRequest.kMatchId] = matchId
        data[GetMatchInfoRequest.kParams] = param
        
        super.init()
    }
    
    // MARK: - Methods
    override func dictionaryFromSelf() -> [String : AnyObject] {
        return self.data as [String : AnyObject]
    }
}
