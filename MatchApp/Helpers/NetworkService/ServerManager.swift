//  ServerManager.swift
//  Created by Meri

import Alamofire

class ServerManager {
    
    private init() {}
    
    static public let shared = ServerManager()
    
    private var headers: HTTPHeaders {
        get {
            let httpHeaders : HTTPHeaders = ["Content-Type" : "application/json",
                                             "Authorization" : "Bearer \(ConfigDataProvider.accessToken ?? "")"]
                                             
            return httpHeaders
        }
    }
    
    private func sendRequest(urlString: String = ConfigDataProvider.baseUrl,
                             path: String,
                             method: HTTPMethod,
                             params: Parameters?,
                             category: ErrorCategory = .default,
                             completion: @escaping (([String : AnyObject]?, ApiError?) -> Void)) {
        guard Reachability.isConnectedToNetwork() == true else {
            completion(nil, ApiError.internetConnectionError())
            return
        }
        let newUrlString = "\(urlString)\(path)"
        print("REQUEST TO : \(newUrlString)")
        
        
        AF.request(newUrlString, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).response { response in
            self.handleResponse(response, category: category, completion: completion)
        }
    }
    private func handleResponse(_ response: AFDataResponse<Optional<Data>>, category: ErrorCategory, completion: @escaping (([String : AnyObject]?, ApiError?) -> Void)) {
        var resp: [String: Any]! = nil
        
        if response.data != nil {//&& !parseAsXml {
            do {
                let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                if let object = json as? [String: Any] {
                 //   print("Parse from JSON : \(object)")
                    resp = object
                }
                if resp == nil {
                    if let obj = json as? [[String: Any]] {
                     //   print("Parse from JSON : \(object)")
                        resp = ["data": obj]
                    }
                }
                
                
            } catch {
                print(error)
            }
        }
        
//        if response.data != nil && parseAsXml {
//            do {
//                let dict = try XMLReader.dictionary(forXMLData: response.data)
//               // print("Parse from XML : \(dict)")
//                resp = dict as? [String:Any]
//            } catch {
//                print("Failed")
//            }
//        }
        
        let code = response.response?.statusCode ?? 999
        
        switch response.result {
        case .success(_):
            if code == 204 {
                completion([String : AnyObject](), nil)
            } else if code == 200 || code == 201 {
                if resp != nil {
                    if let err = ApiError.error(dict: resp! as Dictionary<String, AnyObject>) {
                        completion(nil, err)
                        return
                    }
                    completion(resp! as [String : AnyObject]?, nil)
                } else {
                    completion(nil, ApiError.cantParseError())
                }
            } else if code == 401 {
                var message = ""
                if resp != nil {
                    if let mes = resp["message"] {
                        message = mes as! String
                    }
                }
                let error = ApiError.httpError(code: code)
                completion(nil, error)
                
                let keyWindow = UIApplication.shared.keyWindow
                if var topController = keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    
                    //                        if topController.isKind(of: UINavigationController.classForCoder()) {
                    //                            let topVC = topController as! UINavigationController
                    //                            AppManager.shared.logout(topVC)
                    //                        }
                }
                
            } else {
                var message = ""
                if resp != nil {
                    if let mes = resp["message"] {
                        message = mes as! String
                    }
                    
                    if let data = resp["data"] {
                        let dict = data as! [String: AnyObject]
                        if let mes = dict["message"] {
                            message = mes as! String
                        }
                    }
                }
                let error = ApiError.httpError(code: code)
                completion(nil, error)
            }
        case .failure(let errorValue):
            var err: ApiError
            if let code = response.response?.statusCode {
                err = ApiError.httpError(code: code)
            } else {
                err = ApiError(title: "Error", message: errorValue.localizedDescription)
            }
            completion(nil, err)
        }
    }
    
    private func arrayFrom(_ response: [String: AnyObject]) -> [[String: AnyObject]] {
        return response["payload"] as? [[String : AnyObject]] ?? [[String: AnyObject]]()
    }
    
    // MARK: - Login Request
    func getMatchInfo(with params: GetMatchInfoRequest,
               successBlock: @escaping (_ response: MatchModel) -> (),
               failtureBlock: @escaping (_ error: ApiError) -> ()) {
        
        sendRequest(path: Constants.getMatchInfo,
                    method: .post,
                    params: params.dictionaryFromSelf(),
                    category: .default) { (response, error) in
                        if let error = error {
                            failtureBlock(error)
                        } else {
                            successBlock(MatchModel(dictionary: response!))
                        }
        }
    }
    
    func getVideoUrl(with params: VideoUrlsRequest,
                     successBlock: @escaping (_ response: [VideoUrlsModel]) -> (),
                     failtureBlock: @escaping (_ error: ApiError) -> ()) {
        
        sendRequest(path: Constants.videoUrls,
                    method: .post,
                    params: params.dictionaryFromSelf(),
                    category: .default) { (response, error) in
            if let error = error {
                failtureBlock(error)
            } else {
                var arr: [VideoUrlsModel] = []
                if let data = response?["data"] {
                    let dataArr: [[String : AnyObject]] = data as! [[String : AnyObject]]
                    for dict in dataArr {
                        let model = VideoUrlsModel(dictionary: dict)
                        arr.append(model)
                    }
                }
                successBlock(arr)
            }
        }
    }
}
