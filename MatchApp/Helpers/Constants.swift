//
//  Constants.swift
//  BabyFeedTrackAndReminder
//
//  Created by Meri on 8/20/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    // MARK: - Api url paths
    static let getMatchInfo = "/data"
    static let videoUrls = "/video-urls"

    static let kGetMatchInfo = "get_match_info"
}
