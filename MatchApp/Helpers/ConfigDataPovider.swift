//  ConfigDataPovider.swift
//  Created by Meri

import Foundation

struct ConfigDataProviderKey {
    static let kAccessTokenKey = "AccessToken"
}

struct Notifications {
    
}

class ConfigDataProvider {
    class var baseUrl: String {
        return "https://api.instat.tv/test"
    }
    
    class var accessToken: String? {
        get {
            let token = UserDefaults.standard.string(forKey: ConfigDataProviderKey.kAccessTokenKey)
            return token
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ConfigDataProviderKey.kAccessTokenKey)
            UserDefaults.standard.synchronize()
        }
    }
}
