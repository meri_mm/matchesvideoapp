//MARK:- Extension

import Foundation
import UIKit

extension String {
    func getDateString(formatterFrom: String, formatterTo: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatterFrom
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = formatterTo
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
}

extension UIView {
    var width: CGFloat {
        return frame.size.width
    }
    var height: CGFloat {
        return frame.size.height
    }
}
