//
//  ViewController.swift
//  MatchApp
//
//  Created by Meri Manucharyan on 12.11.21.
//

import UIKit
import AVKit
enum Language {
    case ru
    case us
}

class MatchViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var usButton: UIButton!
    @IBOutlet weak var ruButton: UIButton!
    
    
    @IBOutlet weak var tournamentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!

    @IBOutlet weak var team1Logo: UIImageView!
    @IBOutlet weak var team1NameLabel: UILabel!
    @IBOutlet weak var team1HeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var team2Logo: UIImageView!
    @IBOutlet weak var team2NameLabel: UILabel!
    @IBOutlet weak var team2HeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var videosCollView: UICollectionView!
    @IBOutlet weak var spinner:   UIActivityIndicatorView?

    // MARK: - Properties
    fileprivate let matchPresenter = MatchPresenter(matchService: MatchService())
    var language: Language = Language.ru
    var matchModel: MatchModel?
    var videoUrls: [VideoUrlsModel] = []

    // MARK: - Life Cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configParams()
        
    }

    // MARK: - Methods
    private func configParams() {
        //                tableView?.dataSource = self
        // Set collection view configs
        videosCollView.delegate = self
        videosCollView.dataSource = self
        
        spinner?.hidesWhenStopped = true
        matchPresenter.attachView(false, view: self)
        matchPresenter.getMatches()
        matchPresenter.getVideoUrls()

    }
    
    private func updateMatchInfo() {
        // Set tournament value
        if let tournament: TournamentModel = matchModel?.tournament {
            if language == .ru {
                if let name = tournament.nameRus {
                    tournamentLabel.text = name
                }
            } else {
                if let name = tournament.nameEng {
                    tournamentLabel.text = name
                }
            }
        }
        
        // Set date
        if let dateStr: String = (matchModel?.date) {
            var dateValue = dateStr
            if let str = dateStr.getDateString(formatterFrom: "yyyy-MM-dd HH:mm:ssZ", formatterTo: "yyyy-MM-dd h:mm a") {
                dateValue = str
            }
            dateLabel.text = dateValue
        }
        
        // Set score
        if let team1Sc: Int = matchModel?.team1?.score, let team2Sc: Int = matchModel?.team2?.score {
            let score = "\(team1Sc) : \(team2Sc)"
            scoreLabel.text = score
        }
        
        // Set teams info
        if let team1: TeamModel = matchModel?.team1,  let team2: TeamModel = matchModel?.team2 {
            if language == .ru {
                if let abbrev1 = team1.abbrevRus, let abbrev2 = team2.abbrevRus {
                    team1NameLabel.text = abbrev1
                    team2NameLabel.text = abbrev2
                }
            } else {
                if let abbrev1 = team1.abbrevEng, let abbrev2 = team2.abbrevEng {
                    team1NameLabel.text = abbrev1
                    team2NameLabel.text = abbrev2
                }
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func languageButtonAction(_ sender: UIButton) {
        if sender == ruButton && language != .ru {
            language = .ru
        } else if sender == usButton && language != .us {
            language = .us
        } else {
            return;
        }
        updateMatchInfo()
        videosCollView.reloadData()
    }
    
}

extension MatchViewController: MatchView {
    func startLoading() {
        spinner?.startAnimating()
    }

    func finishLoading() {
        spinner?.stopAnimating()
    }

    func setMatchModel(_ model: MatchModel) {
        matchModel = model
        updateMatchInfo()
    }
    
    func setVideoUrls(_ list: [VideoUrlsModel]) {
        videosCollView?.isHidden = false
        videoUrls = list
        videosCollView.reloadData()
    }
    
    func setEmpty() {
        videosCollView?.isHidden = true
    }
}

extension MatchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VideoCell.cellIdentifier, for: indexPath) as! VideoCell
        var text = "Видео \(indexPath.item+1)"
        if language == .us {
            text = "Video \(indexPath.item+1)"
        }
        cell.titleLabel.text = text
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoModel: VideoUrlsModel = videoUrls[indexPath.row]
        if let urlStr = videoModel.url {
            let player = AVPlayer(url: URL(string: urlStr)!)
            let avPlayerViewController = AVPlayerViewController()
            avPlayerViewController.player = player
            
            present(avPlayerViewController, animated: true) {
                player.play()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = collectionView.width/3
        return CGSize(width: side, height: side)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}
